import fs from 'fs'
import path from 'path'
import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'
import compression from 'compression'
import passport from "passport";
import expressSession from "express-session";
import publicRoutes from './routes/public.js'
import privateRoutes from './routes/private.js'
import accountRoutes from './routes/account.js'
import {
  appLogger,
  metaData,
  requestFileLogger,
  requestConsoleLogger,
} from './logger/index.js';

const port = 3001

const app = express()
const router = express.Router()
const corsOptions = {
  origin: ['http://127.0.0.1:3001', '*.oca-web.com'],
  credentials: true,
  optionsSuccessStatus: 200,
};
app.use(compression());
app.use(cors(corsOptions));
app.use(bodyParser.json())
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(expressSession({
  name: 'auth',
  secret: 'secret',
  resave: false,
  saveUninitialized: false
}))
app.use(passport.initialize())
app.use(passport.session())

// Log into console all 2xx success responses in a short form
app.use(requestConsoleLogger)
// Log into file all responses
app.use(requestFileLogger)

app.get('', async (req, res) => {
  res.sendFile(path.resolve('./build/index.html'))
})
app.get('/', async (req, res) => {
  res.sendFile(path.resolve('./build/index.html'))
})
app.get('/static/js/:file', async (req, res) => {
  const { file } = req.params
  res.sendFile(path.resolve('./build/static/js/' + file))
})
app.get('/static/css/:file', async (req, res) => {
  const { file } = req.params
  res.sendFile(path.resolve('./build/static/css/' + file))
})

router.use('/public', publicRoutes)
router.use('/account', accountRoutes)
router.use('/private', (req, res, next) => {
  if(req.isAuthenticated()) {
    next()
  } else {
    res.redirect('/account/login')
  }
}, privateRoutes)
app.use(router)


let details;
function getDetails() {
  if (details) {
    return details;
  }

  details = JSON.parse(fs.readFileSync(path.resolve('./mocks/productDetails.json')));
  return details;
}

app.get('/products', async (req, res) => {
  const details = getDetails();
  const list = details.map((product) => {
    return {
      id: product.id,
      brand: product.brand,
      model: product.model,
      stock: product.stock,
    };
  });

  res.json({
    products: list
  })
});

app.get('/products/:id', async (req, res) => {
  const details = getDetails();
  const detail = details.filter(product => product.id === parseInt(req.params.id, 10));

  res.json({
    product: detail[0]
  })
});

const server = async () => {
  app.listen(port, () => {
    appLogger.info(`🐱 App listening on port ${port}`, metaData())
  })
}
server().catch((err) => {
  appLogger.error(err);
});

process.on("uncaughtException", function(err) {
  appLogger.error("\n" + (err.stack || err) + "\n", metaData());
  process.exit(1);
});
process.on("unhandledRejection", err => {
  throw err;
});