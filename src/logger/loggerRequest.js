import morgan from 'morgan';
import json from 'morgan-json';
import * as utils from './utils.js';

morgan.token('date', () => new Date().toISOString());
morgan.token('stack', (req, res) => {
  let didEncounterErrorFromRSA = false;
  try {
    const resBody = JSON.parse(res.resBody);
    if (resBody && resBody.errors) {
      didEncounterErrorFromRSA = true;
    }
  } catch (e) {
    // Don't do anything for now
  }

  return res.statusCode > 204 || didEncounterErrorFromRSA
    ? JSON.stringify({
        requestHeaders: req.headers,
        requestBody: req.body,
        responseBodyError: res.resBody,
      })
    : '-';
});

const jsonLogFileFormat = json({
  date: `:date`,
  method: ':method',
  url: ':url',
  status: ':status',
  length: ':res[content-length]',
  'response-time': ':response-time',
  'remote-addr': ':remote-addr',
  'remote-user': ':remote-user',
  'http-version': ':http-version',
  referrer: ':referrer',
  'user-agent': ':user-agent',
  stack: ':stack',
});

const skip = (req) => req.originalUrl.includes('server-health');
export const requestConsoleLogger = morgan(jsonLogFileFormat, { skip });
export const requestFileLogger = morgan(jsonLogFileFormat, {
  stream: utils.requestLogStream,
  skip,
});
