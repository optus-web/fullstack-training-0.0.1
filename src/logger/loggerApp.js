import winston from 'winston';
import DailyRotateFile from 'winston-daily-rotate-file';
import getCallerFile from 'get-caller-file';
import * as LoggerUtils from './utils.js';
const WinstonFormat = winston.format;

export const metaData = () => ({
  date: new Date().toISOString(),
  processFileName: relativeTo(getCallerFile()),
})
const relativeTo = (fullFilePath = '', trimBefore = 'src') => {
  const start = fullFilePath.indexOf(trimBefore) + trimBefore.length + 1;
  return fullFilePath.substring(start);
};

const appConsoleFormatter = WinstonFormat.printf((infoDetails) => {
  const { level, message, ...meta } = { ...metaData(), ...infoDetails };
  const { date, processFileName } = meta;
  return `[${date}, ${level}, ${processFileName}]: ${message}`;
});

const appLogFileFormatter = WinstonFormat.printf((infoDetails) => {
  const { level, message, ...meta } = { ...metaData(), ...infoDetails };
  const { date, processFileName } = meta;

  return JSON.stringify({
    date,
    level,
    processFileName,
    message,
  });
});

const loggingColors = {
  emerg: 'bold magenta',
  alert: 'bold magenta',
  crit: 'bold magenta',
  error: 'bold red',
  warning: 'bold yellow',
  notice: 'bold cyan',
  info: 'bold green',
  debug: 'bold blue',
};

export const appLogger = winston.createLogger({
  level: 'error',
  format: WinstonFormat.json(),
  transports: [
    new winston.transports.Console({
      level: 'info',
      format: WinstonFormat.combine(
        WinstonFormat.colorize({ all: true, colors: loggingColors }),
        WinstonFormat.timestamp({
          format: 'MMM-DD-YYYY HH:mm:ss.SSS',
        }),
        appConsoleFormatter,
      ),
    }),
    new DailyRotateFile({
      filename: `./logs/%DATE%-app.log`,
      datePattern: 'YYYYMMDD-HH',
      maxFiles: 5,
      frequency: '1h',
      level: 'info',
      auditFile: LoggerUtils.pathAppLogAudit,
      format: WinstonFormat.combine(WinstonFormat.json(), appLogFileFormatter),
    }),
  ],
});
