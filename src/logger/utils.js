import rfs from 'rotating-file-stream';

export const requestLogStream = rfs.createStream('access.log', {
  path: './logs',
  maxFiles: 5,
  interval: '1h',
})
