import express from 'express'
import passport from 'passport'
import passportLocal from 'passport-local'
import jwt from 'jsonwebtoken'
import cookie from 'cookie'
const LocalStrategy = passportLocal.Strategy
const router = express.Router()
const version = process.env.npm_package_version
const signingKey = 'secret-signing-key'

const mockDB = [{
  id: 1,
  user: 'Harry Styles',
  pass: '5106958a-6feb-4490-b37f-8048849af113',
}, {
  id: 2,
  user: 'Zayn',
  pass: '91a005b7-f4be-4fcd-b2cb-c857fc457ec0'
}, {
  id: 3,
  user: 'Niall Horan',
  pass: '576312c5-c645-41c3-9cf5-824d574c2ce7'
}, {
  id: 4,
  user: 'Michelle Williams',
  pass: '3d265370-1f7a-46ca-bc5c-0dc0a873c8b4'
}, {
  id: 5,
  user: 'Kelly Rowland',
  pass: '9bd7ea63-604c-4537-971a-765302eaaaa8'
}, {
  id: 6,
  user: 'Beyonce',
  pass: '32a70438-7ef8-4e76-993b-e919626e7d20'
}]
/***
 * Passport conventions
 */

const sign = (payload, options) => {
  return jwt.sign(payload, signingKey, options)
}
const verify = (token) => {
  return jwt.verify(token, signingKey)
}
passport.use(new LocalStrategy(
  function(username, password, done) {
    const user = mockDB.find((user) => user.user === username)
    if (user.pass === password) {
      return done(null, user);
    } else {
      return done(null, false, { message: 'Invalid username or password' });
    }
  }
))
passport.serializeUser(function(user, done) {
  done(null, user.id);
})

passport.deserializeUser(function(id, done) {
  const user = mockDB.find((user) => user.id === id)
  if (user) {
    done(null, user)
  } else {
    done(null)
  }
})


/***
 * Routes
 */
router.post('/login',
  passport.authenticate('local', { failureRedirect: '/account/login', failureMessage: true }),
  function(req, res) {
    const { user } = req
    const token = sign({
      user,
      roles: ['READ', 'WRITE']
    }, {
      expiresIn: 24 * 60 * 60,
      audience: 'Optus',
      jwtid: 'web-jwt'
    })
    res.cookie('jwt', token, {
      httpOnly: true
    })
    res.redirect('/account/profile?' + req.user.user);
  });


router.get('/login', async (req, res) => {
  res.send('Please log in');
});

router.get('/logout', function(req, res) {
  req.logout(function(err) {
    if (err) { return next(err); }
    res.redirect('/account/login');
  });
});

router.get('/profile', async (req, res) => {
  if (req.isAuthenticated()) {
    const cookies = cookie.parse(req.headers.cookie || '')
    const token = cookies['jwt']
    const { roles } = verify(token)

    res.send(`Welcome to your profile page. Your roles are: ${roles.join()}`);
  } else {
    res.redirect('/account/login');
  }
});

/**
 * Try POST the following:
  {
     "user": "ACCCCCCCCCCCCCCCCCCCCCCCCCCCCX"
  }
 *
 * ref: https://security.snyk.io/vuln/SNYK-JS-LODASH-1018905
 */
router.post('/verify', async (req, res) => {
  const { user } = req.body
  const valid = /A(B|C+)+D/.test(user)
  res.json({
    valid,
    version
  })
})

export default router;

