import express from 'express'
const router = express.Router()

const mockProducts = [{
  id: "3e32a1e4-582a-4928-9969-3864d11c8b78",
  name: 'Galaxy S23'
}, {
  id: "0c817713-8786-49c2-a080-2c7376830fe4",
  name: 'iPhone 14'
}]

router.get('/search', async (req, res) => {
  const { keyword } = req.params

  return mockProducts
});

router.get('/product/list', async (req, res) => {
  res.json({
    items: mockProducts
  })
});

router.get('/product/:id', function(req, res) {
  const { id } = req.params
  const product = mockProducts.find((product) => product.id === id)

  res.json(product)
});


export default router;

