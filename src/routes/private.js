import express from 'express'
const router = express.Router()

const mockOrders = [[{
  id: "17f04f98-5738-47be-85dc-e9cbc8de9a24",
  name: 'iPhone 14'
}, {
  id: "5f607e72-ab2d-4525-bbe6-7677091e5509",
  name: 'Optus Plus Promo Plan per month'
}], [{
  id: "35c44cd9-e1fc-447f-8d1c-56c82d1d55a7",
  name: 'Philips Hue Outdoor Motion Sensor'
}]]

router.get('/order/list', async (req, res) => {
  res.json({
    items: mockOrders
  })
});

router.get('/order/:id', function(req, res) {
  const { id } = req.params
  const order = mockOrders.find((order) => order.find(o => o.id === id))

  res.json(order)
});

export default router;