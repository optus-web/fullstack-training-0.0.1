var couchbase = require('couchbase')

async function main() {
  const clusterConnStr = 'couchbases://couch.oca-web.com'
  const username = 'admin'
  const password = 'password'
  const bucketName = 'workshop'

  const cluster = await couchbase.connect(clusterConnStr, {
    username: username,
    password: password,
    // Sets a pre-configured profile called "wanDevelopment" to help avoid latency issues
    // when accessing Capella from a different Wide Area Network
    // or Availability Zone (e.g. your laptop).
    configProfile: 'wanDevelopment',
  })

  const bucket = cluster.bucket(bucketName)

  // Get a reference to the default collection, required only for older Couchbase server versions
  const defaultCollection = bucket.defaultCollection()

  const collection = bucket.scope('couch').collection('test')

  const user = {
    type: 'user',
    name: 'Michael',
    email: 'michael123@test.com',
    interests: ['Swimming', 'Rowing'],
  }

  // Create and store a document
  await collection.upsert('michael123', user)

  // Load the Document and print it
  // Prints Content and Metadata of the stored Document
  let getResult = await collection.get('michael123')
  console.log('Get Result: ', getResult)

  // Perform a SQL++ (N1QL) Query
  const queryResult = await bucket
    .scope('couch')
    .query('SELECT name FROM `test` WHERE country=$1 LIMIT 10', {
      parameters: ['United States'],
    })
  console.log('Query Results:')
  queryResult.rows.forEach((row) => {
    console.log(row)
  })
}

// Run the main function
main()
  .catch((err) => {
    console.log('ERR:', err)
    process.exit(1)
  })
  .then(process.exit)